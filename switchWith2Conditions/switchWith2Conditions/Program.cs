﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace switchWith2Conditions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("[1] - Blue");
            Console.WriteLine("[2] - Red");
            Console.WriteLine("\nPlease enter an option: ");

            var option = Console.ReadLine();

            switch (option)
            {
                case "1":

                    Console.WriteLine($"\nYou chose blue - the sky is blue");

                    break;

                case "2":

                    Console.WriteLine("\nYou chose red - apples can be red");

                    break;

                default:

                    Console.WriteLine("\nErr: Invalid option!");

                    break;
            } // End case Switch
        }
    } // End class Program
}
